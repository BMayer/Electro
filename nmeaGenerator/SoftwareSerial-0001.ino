/* SoftwareSerial-001.ino */
#include <SoftwareSerial.h>

/* Les IO s'agitent-elles frenetiquement ? */

const byte rxPin = 10;
const byte txPin = 11;

SoftwareSerial mySerial(rxPin, txPin);

void setup() {
    pinMode(rxPin, INPUT);
    pinMode(txPin, OUTPUT);
    mySerial.begin(300);
}

void loop() {
    mySerial.println("Hello world !");
}
