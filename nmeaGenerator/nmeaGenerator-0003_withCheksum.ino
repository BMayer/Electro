/*
$IIDPT,0005.0,,*77
$IIDBT,0016.4,f,0005.0,M,,*7F
$GPGSV,3,2,12,15,03,051,44,16,69,183,47,18,10,234,33,20,47,097,50*7E
$IIMTA,20.6,C*01
$IIMTW,21.7,C*17
$GPGSA,A,3,18,21,16,20,26,27,07,08,10,,,,1.92,1.00,1.63*0D
*/

/*
Defaut de mise en forme du checksum :
- Les lettres sont en minuscules
- Si le checksum n'est constitué que d'une lettre, elle n'est pas precedee du caractere 0 (zero)
https://forum.arduino.cc/t/print-hex-with-leading-zeroes/38203
*/

void setup() {
    Serial.begin(4800);
}

void printSentenceWithChecksum(String sentence, bool printLogs) {
    String sentenceWithChecksum = withChecksum(sentence, printLogs);
    Serial.println(sentenceWithChecksum);
}

String withChecksum(String sentence, bool printLogs) {
  // http://www.hhhh.org/wiml/proj/nmeaxor.html
  bool started = false;
  char checksum = 0;
  for (unsigned int index = 0; index < sentence.length(); index++) {
    if (index > 0 && sentence[index - 1] == '$') {
      if (printLogs) Serial.println("Found first checksum char:");
      if (printLogs) Serial.println(sentence[index]);
      if (printLogs) Serial.println(sentence[index], HEX);
      if (printLogs) Serial.println("Set as initial 'last step result'.");
      if (printLogs) Serial.println();
      checksum = sentence[index];
      started = true;
      continue; // Skip the rest of this loop iteration.
    }
    
    if (sentence[index] == '*') {
      if (printLogs) Serial.println("Reached the end of checksum chars.");
      if (printLogs) Serial.println("Final checksum:");
      if (printLogs) Serial.println(checksum, HEX);
      if (printLogs) Serial.println();
      break; // Exit the loop.
    }
    
    // Ignore everything preceeding '$'.
    if (!started) {
      continue; // Skip the rest of this loop iteration.
    }
    
    if (printLogs) Serial.println("Xoring last step result and current char.");
    if (printLogs) Serial.println(checksum, HEX);
    if (printLogs) Serial.println(sentence[index]);
    if (printLogs) Serial.println(sentence[index], HEX);
    
    checksum = checksum xor sentence[index];
    if (printLogs) Serial.println("Got new last step result:");
    if (printLogs) Serial.println(checksum, HEX);
    if (printLogs) Serial.println();
  }
  
  //String sentenceWithChecksum = sentence + (checksum < 10 ? "0" : "") + String(checksum, HEX);
  String sentenceWithChecksum = sentence + (checksum < 0x10 ? "0" : "") + String(checksum, HEX);
  if (printLogs) Serial.println("Sentence with checksum:");
  if (printLogs) Serial.println(sentenceWithChecksum);
  return sentenceWithChecksum;
}

void loop() {
    Serial.println("> $IIDPT,0005.0,,*77");
    printSentenceWithChecksum("$IIDPT,0005.0,,*", false);

    Serial.println("> $IIDBT,0016.4,f,0005.0,M,,*7F");
    printSentenceWithChecksum("$IIDBT,0016.4,f,0005.0,M,,*", false);

    Serial.println("> $GPGSV,3,2,12,15,03,051,44,16,69,183,47,18,10,234,33,20,47,097,50*7E");
    printSentenceWithChecksum("$GPGSV,3,2,12,15,03,051,44,16,69,183,47,18,10,234,33,20,47,097,50*", false);

    Serial.println("> $IIMTA,20.6,C*01");
    printSentenceWithChecksum("$IIMTA,20.6,C**", false);

    Serial.println("> $IIMTW,21.7,C*17");
    printSentenceWithChecksum("$IIMTW,21.7,C*", false);

    Serial.println("> $GPGSA,A,3,18,21,16,20,26,27,07,08,10,,,,1.92,1.00,1.63*0D");
    printSentenceWithChecksum("$GPGSA,A,3,18,21,16,20,26,27,07,08,10,,,,1.92,1.00,1.63*", false);   
}