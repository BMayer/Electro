= Générateur de flux VE Direct pour un SmartShunt Victron

Version *0.1.0* _20240514_

Basé sur la capture depuis un SmartShunt Victron en mode TEXT.
Des fichiers de log sont présents ici.
Le programme est écrit en micropython, testé sur un Raspberry Pico (RP2).

Voici un échantillon recueilli : 

----
PID     0xC030
V       13242
VS      1
I       2526
P       33
CE      0
SOC     1000
TTG     -1
Alarm   OFF
AR      0
BMV     SmartShunt 500A/50mV
FW      0416
MON     0
----
