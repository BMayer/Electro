"""
##  FakeSmartShuntGenerator  ##
Genere du protocole VEDirect text
pour simuler un smartShunt Victron
L'equipement Victron émet
<00> puis
une boucle sur serie de phrases dont la premiere suit le format :
<0d><0a>PID<09>0xFFFF
<0d><0a>V<09>(valeur en millivolts)
<0d><0a>VS<09>(valeur en millivolts)
<0d><0a>I<09>(valeur en milliamperes)
<0d><0a>SOC<09>(valeur en milliemes ? en dix milliemes)
<0d><0a>TTG<09>(valeur en minutes ? secondes)
et de nouveau boucle en commencant par PID
<0d><0a>PID<09>0xFFFF
etc
Les valeurs varient aleatoirement.
Les valeurs I de -30000 a 111000
Les valeurs V de -11000 a 13800
Les valeurs SOC de 0 a 1000
Les valeurs TTG de 0 a 9999
"""

from machine import UART, Pin
import random, time

Imin   = -123000; Imax   = 101000
##  Imin   = -30000; Imax   = 111000 fait que random retourne trop de valeurs positives,
##  alors que, pour gérer l'affichage, il y a besoin d'une majorité de valeur négatives
##  En consequence, on va generer des valeurs insensees, MAIS utiles pour controler l'affichage.
Vmin   = 11000;  Vmax   = 13800
SOCmin = 0;      SOCmax = 1000
TTGmin = -1;     TTGmax = 86400

"""
PID     0xC030
V       13242
VS      1
I       2526
P       33
CE      0
SOC     1000
TTG     -1
Alarm   OFF
AR      0
BMV     SmartShunt 500A/50mV
FW      0416
MON     0
"""

def vedPrint(k, v) :
    print("\x0d\x0a" + k + "\t" + v, end='', sep='')

def vedFormat(k, v) :
    return "\x0d\x0a" + k + "\t" + v

############
##  MAIN  ##
############
    
uart1 = UART(1, baudrate=19200, tx=Pin(4), rx=Pin(5), timeout = 10, timeout_char = 10)
uart1 = UART(1, baudrate=19200, tx=Pin(8), rx=Pin(9), timeout = 10, timeout_char = 10)
uart1.init(bits=8, parity=None, stop=1)

uart1.write("\00")
uart1.flush()
while True :
    time.sleep_ms(100)
    V = random.randrange(Vmin, Vmax, 10)
    ##  Imin   = -30000; Imax   = 111000 fait que random retourne trop de valeurs positives,
    ##  alors que pour gérer l'affichage, il y a besoin d'une majorité de valeur négatives
    I = random.randrange(Imin, Imax)
    P = str(V * I)[0:-6]
    SOC = random.randrange(SOCmin, SOCmax, 10)
    TTG = random.randrange(TTGmin, TTGmax, 10)
    
    lineOut  = vedFormat("PID", "0x9999")
    lineOut += vedFormat("V", str(V))
    lineOut += vedFormat('VS', '1')
    lineOut += vedFormat("I", str(I))
    lineOut += vedFormat('P', P)
    lineOut += vedFormat('CE', '0')
    lineOut += vedFormat('SOC', str(SOC))
    lineOut += vedFormat('TTG', str(TTG))
    lineOut += vedFormat('Alarm', 'OFF')
    lineOut += vedFormat('AR', '0')
    lineOut += vedFormat('BMV', 'SmartShunt 500A/50mV')
    lineOut += vedFormat('FW', '0416')
    lineOut += vedFormat('MON', '0')
    uart1.write(lineOut)
    uart1.flush()
    #print(lineOut)
