// https://www.schnaps.fr/articles.php?lng=fr&pg=1076&prt=1

//En déclaratif

// BATTERIE
#include "VEDirect.h"
float tensionbat0;
float tensionbat1;
float puissance;
float courant;
// 32 bit ints to collect the data from the device
int32_t VE_soc, VE_power, VE_voltage, VE_voltage_moteur, VE_current;

// VEDirect instantiated with relevant serial object
VEDirect myve(Serial2);


void setup() {
    Serial2.begin(38400); // entrée SmartShunt
}


void loop() {

//************************************************************
//  PHRASE NMEA POUR BATTERIE(S) EN XDR
//************************************************************
     
// PHRASES NMEA issues du plugin Enginedata (pour servir directement dans le plugin et/ou dans le DASHBOARD d'OpenCPN qu'il faudra dans ce cas modifier)

//   BATTERY#0 => (Moteur), BATTERY#1 => (Servitude)
//   $IIXDR,U,12.56,V,BATTERY1*1F
//   $IIXDR,U,13.54,V,BATTERY0*1D

// ****  BATTERY#1 => (Servitude)  VE_VOLTAGE  tension1    *****

     char batterieSentence1 [35];    
     PString strbat1(batterieSentence1, sizeof(batterieSentence1));    
     strbat1.print("$IIXDR,");
     strbat1.print("U,");         
     strbat1.print(tensionbat1,2);     
     strbat1.print(",V");   
     strbat1.print(",BATTERY1*");       
     cst = checksum(batterieSentence1);  
     if (cst < 0x10) strbat1.print('0');  
     strbat1.print(cst, HEX);  
     Serial.println(batterieSentence1);   
     Serial3.println(batterieSentence1);

// ****  BATTERY#0 => (Moteur)  VE_VOLTAGE_MOTEUR  tension0    *****

     char batterieSentence0 [35];    
     PString strbat0(batterieSentence0, sizeof(batterieSentence0));    
     strbat0.print("$IIXDR,");
     strbat0.print("U,");         
     strbat0.print(tensionbat0,2);     
     strbat0.print(",V");   
     strbat0.print(",BATTERY0*");       
     cst = checksum(batterieSentence0);  
     if (cst < 0x10) strbat0.print('0');  
     strbat1.print(cst, HEX);  
     Serial.println(batterieSentence0);   
     Serial3.println(batterieSentence0);

}

//*********************************************************
//           LECTURE TENSION BATTERIE
//*********************************************************
void resteVolt()   // <====  N.B.: à supprimer si pas de déclaration d'appel dans le loop et donc supprimer 2 accolades
{
  // Read the data
  if(myve.begin()) {                  // test connection
    VE_soc = myve.read(VE_SOC);
    VE_power = myve.read(VE_POWER);
    VE_voltage = myve.read(VE_VOLTAGE);
    VE_voltage_moteur = myve.read(VE_VOLTAGE_MOTEUR);    
    VE_current = myve.read(VE_CURRENT);
  } else {
    Serial.println("Rien trouvé pour la batterie...");
    //while (1);
  }

 // BATTERIE SERVITUDE
  String myString = String(VE_voltage);
  int length1 = myString.length();

  if (length1 > 4){
    tensionbat1 = VE_voltage*0.001;
  }
    else if (length1 == 4)
    {
      tensionbat1 = VE_voltage*0.01;
    }
        else if (length1 == 3)
      {
        tensionbat1 = VE_voltage*0.1;    
      }
         else if (length1 == 2)
      {
        tensionbat1 = VE_voltage;    
      }

 // BATTERIE MOTEUR
  String myString0 = String(VE_voltage_moteur);
  int length0 = myString0.length();

  if (length0 > 4){
    tensionbat0 = VE_voltage_moteur*0.001;
  }
    else if (length0 == 4)
    {
      tensionbat0 = VE_voltage_moteur*0.01;
    }
        else if (length0 == 3)
      {
        tensionbat0 = VE_voltage_moteur*0.1;    
      }
         else if (length0 == 2)
      {
        tensionbat0 = VE_voltage_moteur;    
      }

 //*************************************************************
  String myString2 = String(VE_current);
  int length2 = myString2.length();
    if (length2 > 3){
        courant = VE_current*0.001;
         }
          else if (length2 > 2 && length2 <= 3)
              {
        courant = VE_current*0.01;
    }
          else if (length2 == 2)
              {
        courant = VE_current*0.1;
    }  
  else if (length2 == 1)
              {
        courant = 999;
    }
  }


//Fonction supplémentaire (hors setup et loop)

// ********************************************
// Calcul du CHECKSUM
// ********************************************

byte cs;
byte checksum(char* str) 
{  
    cs=0;  
    for (unsigned int n = 1; n < strlen(str) - 1; n++)   
    {  
        cs ^= str[n];  
    }  
    return cs;  
}