# shunt_19
# Le thread 0 capture les donnees
# le thread 1 calcule les moyennes


from machine import I2C, UART, Pin, Timer
from pico_i2c_lcd import I2cLcd

import sys
import time #utime
import _thread
import random

##  Variables des informations electriques
global Ipeak; Ipeak = 0
global V, TTG, SOC
global listI; listI = list()
global I3; I3 = -1
global I30; I30 = -1
global I300; I300 = -1
global I3600; I3600 = -1
global I86400; I86400 = -1
global SOC; SOC = 0
global TTG; TTG = 0
global listI3; listI3 = list()
global listI30; listI30 = list()
global listI300; listI300 = list()
global listI3600; listI3600 = list()
global listI86400; listI86400 = list()


global btnRazPressed; btnRazPressed = False
def btnRaz_thread() :
    global btnRazPressed
    while True :
        if (btnRaz.value() == 1) :
            btnRazPressed = True
        time.sleep(1)
_thread.start_new_thread(btnRaz_thread, ())

################
##   Timers   ##
################
##  https://www.upesy.fr/blogs/tutorials/timer-raspberry-pi-pico-with-micropython

def handlerTimerCourt(timerCourt) :
    #Calcul de la moyenne de listI --> listI3
    global I3, listI
    if (len(listI) > 0) :
        I3 = sum(listI) / len(listI)
        listI3.append(I3)
    listI.clear()
    print('I3', I3)
    
def handlerTimerMoyen(timerMoyen) :
    #Calcul de la moyenne de listI3 --> listI30
    global I30, listI3
    if (len(listI3) > 0) :
        I30 = sum(listI3) / len(listI3)
        listI30.append(I30)
    listI3.clear()
    print('I30', I30)
    
def handlerTimerLong(timerLong) :
    #Calcul de la moyenne de listI30 --> listI300
    global I300, listI30
    if (len(listI30) > 0) :
        I300 = sum(listI30) / len(listI30)
        listI300.append(I300)
    listI30.clear()
    print(I300)
    
def handlerTimerHeure(timerHeure) :
    #Calcul de la moyenne de listI300 --> listI3600
    global I3600, listI300
    if (len(listI300) > 0) :
        I3600 = sum(listI300) / len(listI300)
        listI3600.append(I3600)
    listI300.clear()

def handlerTimerJour(timerJour) :
    global I86400, listI3600
    if (len(listI3600) > 0) :
        I86400 = sum(listI3600) / len(listI3600)
        # Pas de conservation de I jour
    listI3600.clear()



##############
##   MAIN   ##
##############

timerCourt = Timer(mode=Timer.PERIODIC, period=3000, callback=handlerTimerCourt)     # 3.0sec
timerMoyen = Timer(mode=Timer.PERIODIC, period=30200, callback=handlerTimerMoyen)    # 30.2 sec
timerLong  = Timer(mode=Timer.PERIODIC, period=300300, callback=handlerTimerLong)    # 300.3 sec
timerHeure = Timer(mode=Timer.PERIODIC, period=3600400, callback=handlerTimerHeure)  # 3600.4 sec
timerJour  = Timer(mode=Timer.PERIODIC, period=86405000, callback=handlerTimerHeure) # 86405 sec

while True :
    time.sleep(0.3)
    #print(lineRaw) # .decode("ASCII")
    if (lineRaw[0:2] == b'V\t') :
        V = lineRaw[2:]
    elif (lineRaw[0:2] == b'I\t') :
        I = int(lineRaw[2:])
        Ipeak = min(Ipeak, I)
        listI.append(I)
    elif (lineRaw[0:4] == b'SOC\t') :
        SOC = lineRaw[4:]
    elif (lineRaw[0:4] == b'TTG\t') :
        TTG = lineRaw[4:]
