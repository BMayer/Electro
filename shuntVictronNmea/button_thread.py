import machine
import _thread
import time

led_onboard = machine.Pin(25, machine.Pin.OUT)
button = machine.Pin(21, machine.Pin.IN, machine.Pin.PULL_DOWN)
global button_pressed; button_pressed = False
def button_reader_thread() :
    global button_pressed
    while True:
        if button.value() == 1 :
            button_pressed = True
        time.sleep(1)
        str = '1 button_pressed', button_pressed, button.value(), '*'
        print(str)
_thread.start_new_thread(button_reader_thread, ())

led_onboard.value(1)
while True :
    str = '0 button_pressed', button_pressed, button.value(), '*'
    print(str)
    if button_pressed :
        led_onboard.value(0)
        time.sleep(3)
        button_pressed = False
    time.sleep(1)
    
