= Utiliser les informations générées par un SmartShunt Victron, via sa prise VE Direct

L'idée est de capturer les informations émises par un SmarShunt Victron
et de les envoyer à un afficheur LCD à grands caractères.
Dans un second temps, il sera possible de les mettre en forme pour les envoyer à un système NMEA 0183.

Quelques notes sont disponibles dans un fichier séparé : link:shunt_notes.adoc[]

Suivant la façon dont le shunt a été monté, les valeurs d'intensité peuvent arithmétiquement s'inverser ; les valeurs négatives devenir positives, et inversement. +
Amha, il convient que le montage soit conforme à ce qui est prévu par Victron, afin que les calculs soient corrects. +
Par contre, l'utilisateur peut être habitué à lire des valeurs **positives** lorsque l'installation consomme. Alors, pourquoi pas inverser le signe de sens à l'affichage, suivant la lecture d'un cavalier/jumper/switch connecté à une broche GPIO, lors du processus d'initialisation ?
