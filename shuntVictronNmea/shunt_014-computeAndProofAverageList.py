"""
shunt_014.py
Calcul d'une moyenne
Comment cela se comporte sur une liste incomplète ?
listI[4] = None; --> TypeError: unsupported types for __add__: 'int', 'NoneType'
"""

from machine import UART, Pin, Timer
import sys
import time #utime
import _thread
import random

##  Variables des informations electriques
## Variables des informations electriques
global Ipeak; Ipeak = 0.0
global V, TTG, SOC
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 11 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
#global listI; listI = list(numMesure for numMesure in range(nbrMesureMax))
global listI; listI = list()
#for numMesure in range(nbrMesureMax) :
    #listI[numMesure] = random.uniform(-33.3, 22.2)
global listI3; listI3 = list()
global listI30; listI30 = list()
global listI300; listI300 = list()
global listI3600; listI3600 = list()
global listI86400; listI86400 = list()
#print("listI", listI)
#mean = sum(listI) / len(listI) # 55 / 11 -> 5.0
#print("mean", mean)

##  Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html

##  Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/

##  Variables port serie RS232 NMEA
# pin 7 == gp5 == RX
uart1 = UART(1, baudrate=19200, tx=Pin(4), rx=Pin(5), timeout = 10, timeout_char = 10)
uart1.init(bits=8, parity=None, stop=1)

################
##   Timers   ##
################
##  https://www.upesy.fr/blogs/tutorials/timer-raspberry-pi-pico-with-micropython

def handlerTimerCourt(timerCourt) :
    #print("listI", listI, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI --> listI3
    if (len(listI) > 0) :
        I3 = sum(listI) / len(listI)
        listI3.append(I3)
    listI.clear()
    
def handlerTimerMoyen(timerMoyen) :
    #print("listI3", listI3, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI3 --> listI30
    if (len(listI3) > 0) :
        I30 = sum(listI3) / len(listI3)
        listI30.append(I30)
    listI3.clear()
    
def handlerTimerLong(timerLong) :
    print("listI30", listI30, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI30 --> listI300
    if (len(listI30) > 0) :
        I300 = sum(listI30) / len(listI30)
        listI300.append(I300)
    listI30.clear()
    
def handlerTimerHeure(timerHeure) :
    print("listI300", listI300, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI300 --> listI3600
    if (len(listI300) > 0) :
        I3600 = sum(listI300) / len(listI300)
        listI3600.append(I3600)
    listI300.clear()

def handlerTimerJour(timerJour) :
    print("listI3600", listI3600, "Ipeak", Ipeak / 10)
    if (len(listI3600) > 0) :
        I86400 = sum(listI3600) / len(listI3600)
        # Pas de conservation de I jour
    listI3600.clear()

##############
##   MAIN   ##
##############

timerCourt = Timer(mode=Timer.PERIODIC, period=3000, callback=handlerTimerCourt)     # 3.0sec
timerMoyen = Timer(mode=Timer.PERIODIC, period=30200, callback=handlerTimerMoyen)    # 30.2 sec
timerLong  = Timer(mode=Timer.PERIODIC, period=300300, callback=handlerTimerLong)    # 300.3 sec
timerHeure = Timer(mode=Timer.PERIODIC, period=3600400, callback=handlerTimerHeure)  # 3600.4 sec
timerJour  = Timer(mode=Timer.PERIODIC, period=86405000, callback=handlerTimerHeure) # 86405 sec

while True :
    #if (line != None) :
    if uart1.any() :
        #line = uart1.readline() ## None si Victron eteint
        lineRaw = uart1.readline().rstrip()#.decode("ASCII")
        """
        b'OR\t0x00000000\r\n'
        b'Checksum\t\xdb'
        b'\r\n'
        b'PID\t0xA231\r\n'
        b'FW\t0121\r\n'
        b'SER#\tHQ20423JZQE\r\n'
        b'MODE\t2\r\n'
        b'CS\t9\r\n'
        b'AC_OUT_V\t23000\r\n'
        b'AC_OUT_I\t-1\r\n'
        b'V\t12879\r\n'
        b'AR\t0\r\n'
        b'WARN\t0\r\n'
        b'OR\t0x00000000\r\n'
        b'Checksum\t\xd4'
        b'\r\n'
        b'PID\t0xA231\r\n'
        """
        """
        PID     0xC030
        V       13242
        VS      1
        I       2526
        P       33
        CE      0
        SOC     1000
        TTG     -1
        Alarm   OFF
        AR      0
        BMV     SmartShunt 500A/50mV
        FW      0416
        MON     0
        """

        #print(lineRaw) # .decode("ASCII")
        if (lineRaw[0:9] == b'AC_OUT_V\t') :
            V = lineRaw[9:]#.rstrip().decode("ASCII")
            #print("V :", V[0:3] + "." + V[3:])
        elif (lineRaw[0:9] == b'AC_OUT_I\t') :
            #listI[numMesure] = lineRaw[9:]#.rstrip().decode("ASCII")
            I = int(lineRaw[9:])
            if (I == -1) :
                I = 0
            listI.append(I)
            #print("extrait :", I)
            #print("I [", numMesure, "] :", listI[numMesure])
            ##  est-ce pic de conso ?
            Ipeak = max(Ipeak, I)
            #print("Ipeak :", Ipeak)
        elif (lineRaw[0:2] == b'V\t') :
            pass#V = lineRaw[2:]
        elif (lineRaw[0:2] == b'I\t') :
            pass#listI.append(int(lineRaw[2:]))
        elif (lineRaw[0:4] == b'SOC\t') :
            SOC = lineRaw[4:]
        elif (lineRaw[0:4] == b'TTG\t') :
            TTG = lineRaw[4:]
