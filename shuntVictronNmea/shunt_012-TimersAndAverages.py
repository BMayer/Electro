"""
shunt_002.py
Variables globales, partagees entre la partie collecte et la partie affichage
Les calculs sont realises par la partie collecte
Exploration des threads pour faire un generateurs de valeurs FAKE
"""

from machine import UART, Pin, Timer
import sys
import time #utime
import _thread
import random

##  Variables des informations electriques
## Variables des informations electriques
global Ipeak; Ipeak = 0.0
global V, TTG, SOC
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 11 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
global I_LISTES; I_LISTES = list(numMesure for numMesure in range(nbrMesureMax))
#for numMesure in range(nbrMesureMax) :
    #I_LISTES[numMesure] = random.uniform(-33.3, 22.2)

##  Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html

##  Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/

##  Variables port serie RS232 NMEA
# pin 7 == gp5 == RX
uart1 = UART(1, baudrate=19200, tx=Pin(4), rx=Pin(5), timeout = 10, timeout_char = 10)
uart1.init(bits=8, parity=None, stop=1)

################
##   Timers   ##
################
##  https://www.upesy.fr/blogs/tutorials/timer-raspberry-pi-pico-with-micropython

##  court terme
def handlerTimerCourt(timerCourt) :
    pass
    #print("Timer COURT ! ! !")
    #print("start apres :", time.ticks_ms())
    #print("absNumMesure : ", absNumMesure)
    
def handlerTimerMoyen(timerMoyen) :
    print("timerMoyen absNumMesure : ", absNumMesure)
    
def handlerTimerLong(timerLong) :
    print("timerLong absNumMesure : ", absNumMesure)

##  Horaire
def handlerTimerHeure(timerHeure) :
    print("timerHeure absNumMesure : ", absNumMesure)

##############
##   MAIN   ##
##############

print(I_LISTES)
numMesure = 3
for i in range(numMesure, numMesure - 7, -1) :
    print(I_LISTES[i], ",", end="")
print("\n- - -")

timerCourt = Timer(mode=Timer.PERIODIC, period=3000, callback=handlerTimerCourt)    # 3.0sec
timerMoyen = Timer(mode=Timer.PERIODIC, period=30200, callback=handlerTimerMoyen)   # 30.2 sec
timerLong  = Timer(mode=Timer.PERIODIC, period=300300, callback=handlerTimerLong)   # 300.3 sec
timerHeure = Timer(mode=Timer.PERIODIC, period=3600400, callback=handlerTimerHeure) # 3600.4 sec

absNumMesure = 0
start = time.ticks_ms() # get millisecond counter
print("Determiner le nombre de mesure en 1 heure, par timer :")


while True :
  
    #if (line != None) :
    
    if uart1.any() :
        #line = uart1.readline() ## None si Victron eteint
        lineRaw = uart1.readline()
        """
        b'OR\t0x00000000\r\n'
        b'Checksum\t\xdb'
        b'\r\n'
        b'PID\t0xA231\r\n'
        b'FW\t0121\r\n'
        b'SER#\tHQ20423JZQE\r\n'
        b'MODE\t2\r\n'
        b'CS\t9\r\n'
        b'AC_OUT_V\t23000\r\n'
        b'AC_OUT_I\t-1\r\n'
        b'V\t12879\r\n'
        b'AR\t0\r\n'
        b'WARN\t0\r\n'
        b'OR\t0x00000000\r\n'
        b'Checksum\t\xd4'
        b'\r\n'
        b'PID\t0xA231\r\n'
        """
        """
        PID     0xC030
        V       13242
        VS      1
        I       2526
        P       33
        CE      0
        SOC     1000
        TTG     -1
        Alarm   OFF
        AR      0
        BMV     SmartShunt 500A/50mV
        FW      0416
        MON     0
        """

        #print(lineRaw) # .decode("ASCII")
        if (lineRaw[0:9] == b'AC_OUT_V\t') :
            V = lineRaw[9:].rstrip().decode("ASCII")
            #print("V :", V[0:3] + "." + V[3:])
        elif (lineRaw[0:9] == b'AC_OUT_I\t') :    
            #print("numMesure :", numMesure)
            if (numMesure < nbrMesureMax - 1) :
                numMesure += 1
            else :
                numMesure = 0
            I_LISTES[numMesure] = lineRaw[9:].rstrip().decode("ASCII")
            #print("I [", numMesure, "] :", I_LISTES[numMesure])
            ##  est-ce pic de conso ?
            Ipeak = max(Ipeak, float(I_LISTES[numMesure]))
            #print("Ipeak :", Ipeak)
            
        absNumMesure += 1
