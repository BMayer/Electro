"""
shunt_015.py
Afficher les informations voulues.
"""

from machine import I2C, UART, Pin, Timer
from pico_i2c_lcd import I2cLcd # import lib

import sys
import time #utime
import _thread
import random

##  Variables des informations electriques
global Ipeak; Ipeak = 0.0
global V, TTG, SOC
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 11 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
#global listI; listI = list(numMesure for numMesure in range(nbrMesureMax))
global listI; listI = list()
#for numMesure in range(nbrMesureMax) :
    #listI[numMesure] = random.uniform(-33.3, 22.2)
global I3
global listI3; listI3 = list()
global listI30; listI30 = list()
global listI300; listI300 = list()
global listI3600; listI3600 = list()
global listI86400; listI86400 = list()
#print("listI", listI)
#mean = sum(listI) / len(listI) # 55 / 11 -> 5.0
#print("mean", mean)

##  Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html

##  Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/

##  Variables port serie RS232 NMEA
# pin 7 == gp5 == RX
uart1 = UART(1, baudrate=19200, tx=Pin(4), rx=Pin(5), timeout = 10, timeout_char = 10)
uart1.init(bits=8, parity=None, stop=1)

##  Partie I2C
#DEFAULT_I2C_ADDR = 0x3f ## (2004A)
DEFAULT_I2C_ADDR = 0x27 ## (1602B)
i2c = I2C(0,scl=Pin(17), sda=Pin(16), freq=400000) # création de l'objet i2C
global lcd
lcd = I2cLcd(i2c, DEFAULT_I2C_ADDR, 4, 20) # création de l'objet LCD i2c

################
##   Timers   ##
################
##  https://www.upesy.fr/blogs/tutorials/timer-raspberry-pi-pico-with-micropython

def handlerTimerCourt(timerCourt) :
    #print("listI", listI, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI --> listI3
    global I3
    if (len(listI) > 0) :
        I3 = sum(listI) / len(listI)
        listI3.append(I3)
    listI.clear()
    #
    print("I3 :", I3, " -*- V", V)
    refreshDisplay()
    
def handlerTimerMoyen(timerMoyen) :
    #print("listI3", listI3, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI3 --> listI30
    if (len(listI3) > 0) :
        I30 = sum(listI3) / len(listI3)
        listI30.append(I30)
    listI3.clear()
    
def handlerTimerLong(timerLong) :
    print("listI30", listI30, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI30 --> listI300
    if (len(listI30) > 0) :
        I300 = sum(listI30) / len(listI30)
        listI300.append(I300)
    listI30.clear()
    
def handlerTimerHeure(timerHeure) :
    print("listI300", listI300, "Ipeak", Ipeak / 10)
    #Calcul de la moyenne de listI300 --> listI3600
    if (len(listI300) > 0) :
        I3600 = sum(listI300) / len(listI300)
        listI3600.append(I3600)
    listI300.clear()

def handlerTimerJour(timerJour) :
    print("listI3600", listI3600, "Ipeak", Ipeak / 10)
    if (len(listI3600) > 0) :
        I86400 = sum(listI3600) / len(listI3600)
        # Pas de conservation de I jour
    listI3600.clear()

def refreshDisplay() :
    lcd.clear() # move_to(col0, li0)
    #lcd.putstr("1234567890123456\naAvV%+-Peak/TTG\n")
    #lcdStr  = "{:.2f}".format(float(V)) + "V "
    #lcdStr += "{:.2f}".format(float(I3)) + "A"
    
    # V :  <class 'str'> ,   I3 : <class 'float'>
    lcdStr = str(int(I3)) + " * " + V.decode("ASCII")
    lcd.putstr(lcdStr)
    



##############
##   MAIN   ##
##############

timerCourt = Timer(mode=Timer.PERIODIC, period=3000, callback=handlerTimerCourt)     # 3.0sec
timerMoyen = Timer(mode=Timer.PERIODIC, period=30200, callback=handlerTimerMoyen)    # 30.2 sec
timerLong  = Timer(mode=Timer.PERIODIC, period=300300, callback=handlerTimerLong)    # 300.3 sec
timerHeure = Timer(mode=Timer.PERIODIC, period=3600400, callback=handlerTimerHeure)  # 3600.4 sec
timerJour  = Timer(mode=Timer.PERIODIC, period=86405000, callback=handlerTimerHeure) # 86405 sec

lcd.backlight_off()

while True :
    #if (line != None) :
    if uart1.any() :
        #line = uart1.readline() ## None si Victron eteint
        lineRaw = uart1.readline().rstrip()#.decode("ASCII")
        """
        PID     0xC030
        V       13242
        VS      1
        I       2526
        P       33
        CE      0
        SOC     1000
        TTG     -1
        Alarm   OFF
        AR      0
        BMV     SmartShunt 500A/50mV
        FW      0416
        MON     0
        """

        #print(lineRaw) # .decode("ASCII")
        if (lineRaw[0:2] == b'V\t') :
            V = lineRaw[2:]
        elif (lineRaw[0:2] == b'I\t') :
            print("  I :", lineRaw[2:])
            I = int(lineRaw[2:])
            Ipeak = min(Ipeak, I)
            listI.append(I)
        elif (lineRaw[0:4] == b'SOC\t') :
            SOC = lineRaw[4:]
        elif (lineRaw[0:4] == b'TTG\t') :
            TTG = lineRaw[4:]
    #else :
        #lcd.clear()
        #lcd.putstr("Plus d'infos ...")
