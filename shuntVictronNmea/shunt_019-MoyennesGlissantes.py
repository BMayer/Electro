"""
shunt_019.py
Calcul des moyennes GLISSANTES
"""

from machine import I2C, UART, Pin, Timer
#from pico_i2c_lcd import I2cLcd
#from collections import deque #pour .popleft()

import sys
import time #utime
#import _thread
import random

##  Variables des informations electriques
global Ipeak; Ipeak = 0
global V, TTG, SOC
global SOC; SOC = 0
global TTG; TTG = 0

global listI; listI=list()

global I3; I3 = None
##  10 mesures de 3sec en 30 secondes
global listI3; listI3 = list(0 for i in range(10))
global I3id; I3id = 0

global I30; I30 = None
##  10 mesures de 30sec en 300 secondes / 5 minutes
global listI30; listI30 = list(0 for i in range(10))
global I30id; I30id = 0

global I300; I300 = None
##  12 mesures de 300sec en 3600 secondes / 1 heure
global listI300; listI300 = list(0 for i in range(12))
global I300id; I300id = 0

global I3600; I3600 = None
##  24 mesures de 3600sec en 86400 secondes / 1 jour
global listI3600; listI3600 = list(0 for i in range(24))
global I3600id; I3600id = 0

global I86400; I86400 = None
global listI86400; listI86400 = list()



################
##   Timers   ##
################
##  https://www.upesy.fr/blogs/tutorials/timer-raspberry-pi-pico-with-micropython

def handlerTimerCourt(timerCourt) :
    #Calcul de la moyenne de listI --> listI3
    global I3, listI, I3id, listI3
    if (len(listI) > 0) :
        I3 = int(sum(listI) / len(listI))
        listI3.append(I3)
        #listI3.popleft()
        del listI3[0]
    listI.clear() 
    #print('I3', I3, 'listI3', listI3)
    
def handlerTimerMoyen(timerMoyen) :
    #Calcul de la moyenne de listI3 --> listI30
    global I30, listI3, I30id, listI30
    if (len(listI3) > 0) :
        I30 = int(sum(listI3) / len(listI3))
        listI30.append(I30)
        del listI30[0]
    #listI3.clear()
    #listI3 = list(0 for i in range(10))
    #print("I30\t", I30, "\tlistI30", listI30)
    
def handlerTimerLong(timerLong) :
    #Calcul de la moyenne de listI30 --> listI300
    global I300, listI30, I300id, listI300
    if (len(listI30) > 0) :
        I300 = int(sum(listI30) / len(listI30))
        listI300.append(I300)
        del listI300[0]
    #listI30 = list(0 for i in range(10))
    print("\t", 'Je tourne depuis', (time.time() - start) / 60, 'minutes')
    print("I300\t", I300, "\tlistI300", listI300)
    
def handlerTimerHeure(timerHeure) :
    #Calcul de la moyenne de listI300 --> listI3600
    global I3600, listI300, I3600id, listI3600
    global start
    if (len(listI300) > 0) :
        I3600 = int(sum(listI300) / len(listI300))
        listI3600.append(I3600)
        del listI3600[0]
    print("\tJe tourne depuis", (time.time() - start) / 60, 'minutes')
    print("I3600\t", I3600, "\tlistI3600", listI3600)

def handlerTimerJour(timerJour) :
    global I86400, listI3600
    if (len(listI3600) > 0) :
        I86400 = int(sum(listI3600) / len(listI3600))
        listI86400.append(I86400)
        del listI86400[0]
        # Pas de conservation de I jour
    print("\tJe tourne depuis", (time.time() - start) / 60, 'minutes')
    print("I86400\t", I86400, "\tlistI86400", listI86400)




##############
##   MAIN   ##
##############

timerCourt = Timer(mode=Timer.PERIODIC, period=3000, callback=handlerTimerCourt)     # 3.0sec
timerMoyen = Timer(mode=Timer.PERIODIC, period=30200, callback=handlerTimerMoyen)    # 30.2 sec
timerLong  = Timer(mode=Timer.PERIODIC, period=300300, callback=handlerTimerLong)    # 300.3 sec
timerHeure = Timer(mode=Timer.PERIODIC, period=3600400, callback=handlerTimerHeure)  # 3600.4 sec
timerJour  = Timer(mode=Timer.PERIODIC, period=86405000, callback=handlerTimerHeure) # 86405 sec


start = time.time()
print('GO !', start)
while True :
    #if (line != None) :
    #if uart1.any() :
    time.sleep(0.5)
    I = random.randrange(-33000, 33000, 10)
    listI.append(I)


print('\n this is the end...')