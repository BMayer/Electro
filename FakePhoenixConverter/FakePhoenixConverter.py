"""
Genere du protocole VEDirect text
pour simuler un smartShunt Victron
L'equipement Victron émet
<00> puis
une boucle sur serie de phrases dont la premiere suit le format :
<0d><0a>PID<09>0xFFFF
<0d><0a>V<09>(valeur en millivolts)
<0d><0a>VS<09>(valeur en millivolts)
<0d><0a>I<09>(valeur en milliamperes)
<0d><0a>SOG<09>(valeur en milliemes ? en dix milliemes)
<0d><0a>TTG<09>(valeur en minutes ? secondes)
et de nouveau boucle en commencant par PID
<0d><0a>PID<09>0xFFFF
etc
Les valeurs varient aleatoirement.
Les valeurs V de 11000 a 13800
Les valeurs I de -30000 a 30000
Les valeurs SOG de 0 a 1000
Les valeurs TTG de 0 a 9999
"""
"""
Imax = 0.0
Vmin = 0.0
Vmax = 0.0
# Les valeurs instantanees I et V se font sur les 3 dernieres valeurs
I = list()
iI = 0
iMaxI = 3
V = list()
iV = 0
iMaxV = 3
# Les valeurs instantanees SOG et TTG se font sur les 30 dernieres valeurs
SOG = 0
iSOG = 0
iMaxSOG = 30
TTG = 0
iTTG = 0
iMaxTTG = 30
"""

import random, time

Imin = -30000
Imax = 30000
Vmin = 11000
Vmax = 13800
SOGmin = 0
SOGmax = 1000
TTGmin = 0
TTGmax = 1440

"""
b'OR\t0x00000000\r\n'
b'Checksum\t\xdb'
b'\r\n'
b'PID\t0xA231\r\n'
b'FW\t0121\r\n'
b'SER#\tHQ20423JZQE\r\n'
b'MODE\t2\r\n'
b'CS\t9\r\n'
b'AC_OUT_V\t23000\r\n'
b'AC_OUT_I\t-1\r\n'
b'V\t12879\r\n'
b'AR\t0\r\n'
b'WARN\t0\r\n'
b'OR\t0x00000000\r\n'
b'Checksum\t\xd4'
b'\r\n'
b'PID\t0xA231\r\n'
"""

############
##  MAIN  ##
############

while True :
    time.sleep(1)
    print("\x0d\x0aPID\t0x9999", end='', sep='')
    print("\x0d\x0aI\t" + str(random.randrange(Imin, Imax, 10)), end='', sep='')
    print("\x0d\x0aV\t" + str(random.randrange(Vmin, Vmax, 10)), end='', sep='')
    print("\x0d\x0aSOG\t" + str(random.randrange(SOGmin, SOGmax, 10)), end='', sep='')
    print("\x0d\x0aTTG\t" + str(random.randrange(TTGmin, TTGmax, 10)), end='', sep='')

