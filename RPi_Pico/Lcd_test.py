"""
https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/
"""

from machine import I2C, Pin # import class I2C
from pico_i2c_lcd import I2cLcd # import lib
import utime

# The PCF8574 has a jumper selectable address: 0x20 - 0x27
DEFAULT_I2C_ADDR = 0x3f ## (2004A) i2c_scan : 63
DEFAULT_I2C_ADDR = 0x27 ## (1602B) i2c_scan : 39
BACKLIGHT = True

"""Test function for verifying basic functionality."""
print("Running test_main")
i2c = I2C(0,scl=Pin(17), sda=Pin(16), freq=400000) # création de l'objet i2C
lcd = I2cLcd(i2c, DEFAULT_I2C_ADDR, 2, 16) # création de l'objet LCD i2c

#lcd.putstr("1234567890123456\n7890123456789012")
lcd.putstr("1234567890123456\naAvV%+-Peak/TTG\n")
#delay(3000)
#lcd.clear()

while True :
    utime.sleep(3)
    lcd.clear() # move_to(col0, li0)
    if (BACKLIGHT) :
        lcd.backlight_off()
        BACKLIGHT = False
    else :
        lcd.backlight_on()
        BACKLIGHT = True
    lcd.putstr("1234567890123456\naAvV%+-Peak/TTG\n")