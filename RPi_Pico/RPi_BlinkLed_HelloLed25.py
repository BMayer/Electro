import machine
import utime

LedOnboard = machine.Pin(25, machine.Pin.OUT)

while True :
    LedOnboard.value(1)
    utime.sleep(3)
    LedOnboard.value(0)
    utime.sleep(3)