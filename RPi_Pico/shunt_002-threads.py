"""
shunt_002.py
Variables globales, partagees entre la partie collecte et la partie affichage
Les calculs sont realises par la partie collecte
Exploration des threads
"""

import sys
import time #utime
import _thread
import random

##  Variables des informations electriques
## Variables des informations electriques
global Imax; Imax = 0.0
global V, TTG, SOC
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 10 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
global I_LISTES; I_LISTES = list(numMesure for numMesure in range(nbrMesureMax))

##  Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html

##  Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/

##  Variables port serie RS232 NMEA

##  Thread collecte
def collecte_thread() :
    global Imax
    global I_LISTES
    global numMesure
    global nbrMesureMax # 0 to numMesureMax - 1
    global V, TTG, SOC
    
    
    #print(l[numMesure])
    
    
    print("collecte")
    Imax = -3.9
    print("new val", Imax)
    
    while True :
        I_LISTES[numMesure] = random.uniform(-11.1, 13.8)
        print("je mesure", I_LISTES[numMesure], "indice", numMesure)
        time.sleep(1)
        numMesure += 1
        if (numMesure == nbrMesureMax) : numMesure = 0


##############
##   MAIN   ##
##############

print("Avant : ", Imax)
time.sleep(1)
_thread.start_new_thread(collecte_thread, ())
time.sleep(1)
print("Apres : ", Imax)

time.sleep(3)
while True :
    print("J'affiche", I_LISTES[numMesure], "indice", numMesure)
    time.sleep(1)
    



