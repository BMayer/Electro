= Le démarrage automatique du RaspberryPi Pico

== boot.py

Est exécuté AVANT main.py


== main.py

Va bien nous embêter, il gêne l'usage d'IDE tel Thonny, car le microcontrôleur fait tourner un programme...

J'ai bien essayé de "booter", par appui sur BOOTSEL puis envoi d'un fichier micropython (*.uf2), mais cela conserve les fichiers en place, tout au moins main.py

Après quelques recherches, j'ai trouvé plusieurs solutions, qui suivent 2 catégories : +
- Des fichiers *.uf2 qui renomment main.py, ou qui effacent la mémoire flash, +
- Des utilitaires (Python) qui permettent de manipuler les fichiers dans la mémoire flash. +

J'ai utilisé la solution à partir de l'utilitaire Python « ampy » gracieusement fourni par l'équipe Adafruit
, et installé sans essayer la solution à base de « rshell ».
[source,bash]
----
$ ampy --port /dev/ttyAMC0 rm main.py
$ ampy --port /dev/ttyAMC0 put main.py
----

La distribution Debian n'aime pas que l'on installe par pip, il convient d'utiliser la syntaxe suivante :
[source,bash]
----
$ pip3 install --break-system-packages adafruit-ampy
$ pip3 install --break-system-packages rshell
----

ampy --> https://www.framboise314.fr/raspberry-pi-pico-la-carte-microcontroleur-de-la-fondation/#Installer_ampy[]

rshell --> https://github.com/dhylands/rshell[] traduction https://wiki.mchobby.be/index.php?title=MicroPython-Hack-RShell[]

D'autres infos glânées de-ci de-là :

* (interruptions) https://wdi.centralesupelec.fr/boulanger/MicroPython/TutorialPyboard[]
* https://wiki.mchobby.be/index.php?title=FEATHER-CHARGER-FICHIER-MICROPYTHON-BOOT[]
* https://micropython.fr/06.technique/acceder_flash/[]
* https://micropython.fr/06.technique/sequence_de_boot/[]
* https://www.editions-eni.fr/livre/micropython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906/sequence-de-demarrage[]
* 
* https://forums.raspberrypi.com/viewtopic.php?t=305432[]
* https://forums.raspberrypi.com/viewtopic.php?t=362442[]
* https://forums.raspberrypi.com/viewtopic.php?t=321332[]
* https://diyprojectslab.com/how-to-delete-main-py-in-raspberry-pi-pico/[]
* https://electrocredible.com/how-to-reset-raspberry-pi-pico-w/[]
* https://forum.micropython.org/viewtopic.php?t=10838[]
* https://cfp.is-a.dev/pico-remove-main[]
* https://arduiblog.com/2021/02/15/raspberry-pico/[]
* https://github.com/thonny/thonny/issues/2843[]
* https://www.reddit.com/r/raspberrypipico/comments/133z4u9/how_to_bypass_mainpy/?tl=fr[]


== ampy

[source,bash]
----
$ ampy --help
Usage: ampy [OPTIONS] COMMAND [ARGS]...

  ampy - Adafruit MicroPython Tool

  Ampy is a tool to control MicroPython boards over a serial connection.
  Using ampy you can manipulate files on the board''s internal filesystem and
  even run scripts.

Options:
  -p, --port PORT    Name of serial port for connected board.  Can optionally
                     specify with AMPY_PORT environment variable.  [required]
  -b, --baud BAUD    Baud rate for the serial connection (default 115200).
                     Can optionally specify with AMPY_BAUD environment
                     variable.
  -d, --delay DELAY  Delay in seconds before entering RAW MODE (default 0).
                     Can optionally specify with AMPY_DELAY environment
                     variable.
  --version          Show the version and exit.
  --help             Show this message and exit.

Commands:
  get    Retrieve a file from the board.
  ls     List contents of a directory on the board.
  mkdir  Create a directory on the board.
  put    Put a file or folder and its contents on the board.
  reset  Perform soft reset/reboot of the board.
  rm     Remove a file from the board.
  rmdir  Forcefully remove a folder and all its children from the board.
  run    Run a script and print its output.
----


== rshell

Traduction de la doc: https://wiki.mchobby.be/index.php?title=MicroPython-Hack-RShell[]

[source,bash]
----
$ rshell --help
usage: rshell [options] [command]

Remote Shell for a MicroPython board.

positional arguments:
  cmd                   Optional command to execute

options:
  -h, --help            show this help message and exit
  -b BAUD, --baud BAUD  Set the baudrate used (default = 115200)
  --buffer-size BUFFER_SIZE
                        Set the buffer size used for transfers (default = 512 for USB, 32 for UART)
  -p PORT, --port PORT  Set the serial port to use (default 'None')
  --rts RTS             Set the RTS state (default '')
  --dtr DTR             Set the DTR state (default '')
  -u USER, --user USER  Set username to use (default 'micro')
  -w PASSWORD, --password PASSWORD
                        Set password to use (default 'python')
  -e EDITOR, --editor EDITOR
                        Set the editor to use (default 'vi')
  -f FILENAME, --file FILENAME
                        Specifies a file of commands to process.
  -d, --debug           Enable debug features
  -n, --nocolor         Turn off colorized output
  -l, --list            Display serial ports
  -a, --ascii           ASCII encode binary files for transfer
  --wait WAIT           Seconds to wait for serial port
  --timing              Print timing information about each command
  -V, --version         Reports the version and exits.
  --quiet               Turns off some output (useful for testing)

You can specify the default serial port using the RSHELL_PORT environment variable.
----
