"""
shunt_001.py
Variables globales, partagees entre la partie collecte et la partie affichage
Les calculs sont realises par la partie collecte
"""

import time #utime
import _thread

## Variables des informations electriques
global Imax; Imax = 0.0
global I_LISTES; I_LISTES = list()
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 10 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
global V, TTG, SOC

l = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

while True :
    if (numMesure == nbrMesureMax) : numMesure = 0
    print(l[numMesure])
    numMesure = numMesure + 1
    time.sleep(1)

## Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html

## Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/
#DEFAULT_I2C_ADDR = 0x27 ## (1602B)
#BACKLIGHT = True
#i2c = I2C(0, scl=Pin(17), sda=Pin(16), freq=400000) # création de l'objet i2C
#lcd = I2cLcd(i2c, DEFAULT_I2C_ADDR, 4, 20) # création de l'objet LCD i2c


## Variables port serie RS232 NMEA




