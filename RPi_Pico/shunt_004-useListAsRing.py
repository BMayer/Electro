"""
shunt_002.py
Variables globales, partagees entre la partie collecte et la partie affichage
Les calculs sont realises par la partie collecte
Exploration des threads pour faire un generateurs de valeurs FAKE
"""

import sys
import time #utime
import _thread
import random

##  Variables des informations electriques
## Variables des informations electriques
global Imax; Imax = 0.0
global V, TTG, SOC
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 33 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
global I_LISTES; I_LISTES = list(numMesure for numMesure in range(nbrMesureMax))
#for numMesure in range(nbrMesureMax) :
    #I_LISTES[numMesure] = random.uniform(-33.3, 22.2)

##  Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html

##  Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/

##  Variables port serie RS232 NMEA



##############
##   MAIN   ##
##############

print(I_LISTES)
numMesure = 3
for i in range(numMesure, numMesure - 7, -1) :
    print(I_LISTES[i], ",", end="")
print("\n- - -")
