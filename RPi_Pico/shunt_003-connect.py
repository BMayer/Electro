"""
shunt_001.py
Variables globales, partagees entre la partie collecte et la partie affichage
Les calculs sont realises par la partie collecte
Connexions ?
"""

from machine import UART, I2C, Pin # import class I2C
from pico_i2c_lcd import I2cLcd # import lib
import utime
import _thread

## Variables des informations electriques
global Imax; Imax = 0.0
global I_LISTES; I_LISTES = list()
global numMesure; numMesure = 0
global nbrMesureMax; nbrMesureMax = 10 # 0 to numMesureMax - 1
# https://docs.python.org/fr/3/library/stdtypes.html#set
#global setMesures; setMesures = set(numMesure for numMesure in range(nbrMesureMax))
global V, TTG, SOC

## Variables connexion equipement Victron
# https://electrocredible.com/raspberry-pi-pico-serial-uart-micropython/
# https://docs.micropython.org/en/latest/library/machine.UART.html
uart = UART(1, baudrate=19200, tx=Pin(4), rx=Pin(5))
uart.init(19200, bits=8, parity=None, stop=1) # init with given parameters

## Variables affichage (a integrer dans le thread affichage)
# Pour la lib, voir https://www.micropython.fr/port_pi_pico/07.librairies/i2c/lcd/
#DEFAULT_I2C_ADDR = 0x27 ## (1602B)
#BACKLIGHT = True
#i2c = I2C(0, scl=Pin(17), sda=Pin(16), freq=400000) # création de l'objet i2C
#lcd = I2cLcd(i2c, DEFAULT_I2C_ADDR, 4, 20) # création de l'objet LCD i2c


## Variables port serie RS232 NMEA




