"""
Scan du bus I2C
https://micropython.fr/modules_center/seriel_i2c/scanner_bus_i2c/
https://projetsdiy.fr - https://diyprojects.io
"""

import machine

sda = machine.Pin(16)
scl = machine.Pin(17)
i2c = machine.I2C(0, sda=sda, scl=scl, freq = 400000)

devices = i2c.scan()

if (len(devices) == 0) :
  print("Rien de detecté")
else :
  print("[" + str(len(devices)) + "] id(s) trouvé(s)")
  for device in devices :
    print("id :", device, "(", hex(device), ")")
    

