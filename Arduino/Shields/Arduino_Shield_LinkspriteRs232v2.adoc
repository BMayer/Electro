= Le shield Linksprite RS232 v2

== Explanation of Jumpers on RS232 Shield V2
https://learn.linksprite.com/arduino/shields/explanation-of-jumpers-on-rs232-shield-v2/[]

Posted by: admin , June 10, 2014

We would like to detail the usage of jumpers on RS232 Shield V2:

(1) About jumpers:  J3 has its pins (D0 to D7) connected together, so is J1.   It  means that all pins of J3 are connected together to “TX” and so is J1.  Then “TX” and “RX” is connected to MAX232 which is used to level conversion.  We must choose two pins among 8 pins.  The decision depend on two pins which you are going to use. Once decided, it is the jumpers that can help you to achieve it.

(2) All pins are suitable for software serial ports. And only MTX and MRX can also be used for hardware serial ports.

(3) S1 will be connected to another RESET of master microcontroller. So the purpose of S1 is to reset a device.

Note:    Pins of J2 are independent, you can choose two among them you want.          Those two pins are regarded as TX and RX.

== forum
https://forum.arduino.cc/t/arduino-uno-linksprite-shield-rs232-v2-send-message-to-rs232/342159/2[]


== forum
https://forum.arduino.cc/t/uno-and-rs232-shield-v2-from-linksprite-help-needed/409132/6[]

